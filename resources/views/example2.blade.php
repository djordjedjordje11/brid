<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">




        <style>
            body {
                font-family: 'Nunito';
            }

            .container {
                width: 100%;
            }
        </style>
    </head>
    <body class="antialiased">

        <div id="Brid_11141755" class="brid" style="width:640;height:360;" > </div>
        <button id="mute">Mute/Unmute</button>
        <input type="text" id="golink">
        
        <script type="text/javascript" src="//services.brid.tv/player/build/brid.min.js"></script>

        <script type="text/javascript">
         $bp("Brid_11141755", {"id":"25265","width":"640","height":"360","video":"705458"});

        // log events
         $bp("Brid_11141755").add(['play', 'pause', 'ended', 'stopped', 'lastSecond', 'mutechange', 'volumechange'], function(e) {
             console.log(e.type);
         })

        // log video info
         $bp("Brid_11141755").add('play', function(e){
             var video = $bp("Brid_11141755");
             console.log(`
Title: ${video.currentSource.title},
Description: ${video.currentSource.description},
Duration: ${video.currentSource.duration},
Tags: ${video.currentSource.tags}
             `)
         })

        //  define variables
         var button = document.getElementById('mute');
         var golink = document.getElementById('golink');
         var mute = false;

        //  mute/unmute
         button.addEventListener('click', function() {
             mute = !mute;
            $bp('Brid_11141755').muted(mute)
         });

         // go to the URL
         golink.addEventListener("keyup", function(event) {
            if (event.keyCode === 13) {
            event.preventDefault();
                var url = golink.value;
                $bp("Brid_11141755").src(`${url}`);
                $bp("Brid_11141755").play();
            }
        });
         </script>
    </body>
</html>
