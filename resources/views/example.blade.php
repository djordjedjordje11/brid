<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


        <style>
            body {
                font-family: 'Nunito';
            }

            .container {
                display: grid;
                grid-template-columns: 20% 20% 20% 20% 20%;
            }

            .content {
                padding: 10px;
                text-align: center;
            }

            .video-image {
                width: 100%;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="container">
            {{-- <div class="content">
                <img class="video-image" src="//cdn.brid.tv/live/partners/18915/thumb/705464_t_1611139344.png">
                <div class="video-name">Name: Bridgerton | Official Trailer | Netflix</div>
                <div class="video-description">Description: Don't have description in API</div>
            </div> --}}
        </div>
        <script>
            $(document).ready(function(){
                $.ajax({
                    url: "/get-api",
                    success: function(result){
                        data = JSON.parse(result);
                        data = data.data
                        console.log(data);
                        for(i=0;i<data.length;i++)
                        {
                            $('.container').append(`
                            <div class="content">
                                <img class="video-image" src="${data[i].Video.thumbnail}">
                                <div class="video-name">Name: ${data[i].Video.name}</div>
                                <div class="video-description">Description: Don't have description in API</div>
                            </div>
                            `)
                        }
                }
                });
            });
        </script>
    </body>
</html>
