<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ExampleController extends Controller
{
    public function apiGet()
    {
        $client = new Client();
        $data = $client->get('https://api.brid.tv/apiv3/video/all/18915.json', ['query' => ['access_token' => 'ca3f7b6843ae84b71da64b1d041c867f12b1aa0b']]);
        return $data->getBody();
    }
}
